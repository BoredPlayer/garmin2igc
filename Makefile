# Compiler and compiler flags
CXX := g++
CXXFLAGS := -std=c++20 -Wall -Werror -D_DEBUG

# Target executable name
TARGET := g2igc

# Source files and object files (considering they are in the 'source' folder)
SRCS := $(wildcard source/*.cpp)
OBJS := $(SRCS:source/%.cpp=build/%.o)

# Default rule to build the executable
all: $(TARGET)

$(TARGET): $(OBJS)
	$(CXX) $(CXXFLAGS) $(OBJS) -o $(TARGET)

# Rule to build object files from source files
build/%.o: source/%.cpp | build
	$(CXX) $(CXXFLAGS) -c $< -o $@

# Create the 'build' directory if it doesn't exist
build:
	mkdir -p build

# Clean rule to remove object files and the executable
clean:
	rm -rf build $(TARGET)

