#include "deviceInfo.h"

deviceInfo::deviceInfo(){
    std::string manufacturer = "Garmin";
    this->unsp = "Unspecified";
    this->setManufacturer(manufacturer);
    this->setPilot(this->unsp);
    this->setGliderType(this->unsp);
    this->setGliderID(this->unsp);
    this->setGPSDatum(0);
    this->setSoftwareVersion(this->unsp);
    this->setHardwareVersion(this->unsp);
    this->setFlightRecorder(this->unsp);
    this->setCompetitionID(this->unsp);
    this->cvDate(1, 1, 1970);
}

void deviceInfo::setManufacturer(std::string manufacturer){
    this->manufacturer = manufacturer;
}

void deviceInfo::setSerialNumber(std::string serial_number){
    this->serial_number = serial_number;
}

void deviceInfo::setPilot(std::string pilot){
    this->pilot = pilot;
}

void deviceInfo::setGliderType(std::string glider_type){
    this->glider_type = glider_type;
}

void deviceInfo::setGliderID(std::string glider_id){
    this->glider_id = glider_id;
}

void deviceInfo::setGPSDatum(char datum_ID){
    switch(datum_ID){
        case 1:
            this->GPS_datum = "WGS-84";
            break;
        default:
            this->GPS_datum = this->unsp;
            break;
    }
}

void deviceInfo::setSoftwareVersion(std::string software_version){
    this->software_version = software_version;
}

void deviceInfo::setHardwareVersion(std::string hardware_version){
    this->hardware_version = hardware_version;
}

void deviceInfo::setFlightRecorder(std::string flight_recorder){
    this->flight_recorder = this->manufacturer+", "+flight_recorder;
}

void deviceInfo::setCompetitionID(std::string competition_id){
    this->competition_id = competition_id;
}

void deviceInfo::setDate(std::string date){
    this->date = date;
}

void deviceInfo::cvDate(int day, int month, int year){
    std::stringstream ms;
    if(year<1849) year+=1970;
    year = year - ((int) year/100)*100;
    //if(year<1970) year+=1970;
    ms << std::setw(2) << std::setfill('0');
    ms << day;
    ms << std::setw(2) << std::setfill('0');
    ms << month;
    ms << std::setw(2) << std::setfill('0');
    ms << year;
    this->setDate(ms.str());
}

std::string deviceInfo::e2igc(){
    std::stringstream ms;
    //A row
    ms << "AXXX" << this->serial_number << std::endl;
    //Headers
    ms << "HFDTE" << this->date << std::endl;
    ms << "HFPLTPILOTINCHARGE: " << this->pilot << std::endl;
    ms << "HFPLTPILOTINCHARGE: " << this->glider_type << std::endl;
    ms << "HFGIDGLIDERID: " << this->glider_id << std::endl;
    ms << "HFDTM100GPSDATUM: " << this->GPS_datum << std::endl;
    ms << "HFRFWFIRMWAREVERSION: " << this->software_version << std::endl;
    ms << "HFRHWHARDWAREVERSION: " << this->hardware_version << std::endl;
    ms << "HFFTYFRTYPE: " << this->flight_recorder << std::endl;
    ms << "HFCIDCOMPETITIONID: " << this->competition_id << std::endl;
    return ms.str();
}