#ifndef __DEVICEINFO_H__
#define __DEVICEINFO_H__

#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>

class deviceInfo{
public:
    deviceInfo();
    void setDate(std::string date);
    void cvDate(int day, int month, int year);
    void setManufacturer(std::string manufacturer);
    void setSerialNumber(std::string serial_number);
    void setPilot(std::string pilot);
    void setGliderType(std::string glider_type);
    void setGliderID(std::string glider_id);
    void setGPSDatum(char datum_ID);
    void setSoftwareVersion(std::string software_version);
    void setHardwareVersion(std::string hardware_version);
    void setFlightRecorder(std::string flight_recorder);
    void setCompetitionID(std::string competition_id);
    std::string e2igc();
private:
    std::string manufacturer;
    std::string serial_number;
    std::string pilot;
    std::string glider_type;
    std::string glider_id;
    std::string GPS_datum;
    std::string software_version;
    std::string hardware_version;
    std::string flight_recorder;
    std::string competition_id;
    std::string date;
    std::string unsp;
};

#endif