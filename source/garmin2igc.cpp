﻿#include <iostream>
#include <math.h>
#include <string>
#include <fstream>
#include <iterator>
#include <vector>
#include <stdlib.h>
#include <sstream>
#include <iomanip>
#include <cstdint>

#include "coordinate.h"
#include "GPSdata.h"
#include "deviceInfo.h"

//TODO: add A and H lines to generated file. Use as much automation
//as possible.

//Finds the key substring in line string. Returns true, if
//key is present and false if it's not.
bool findSubstr(std::string key, std::string line) {
    return line.find(key.c_str()) != std::string::npos;
}

//Finds keyword in given line
std::string extractValue(
    std::string line,
    std::string keyword,
    size_t *pos
){
    size_t spos = line.find(keyword.c_str(), *pos)+keyword.size()+2;
    size_t pend = line.find('\"', spos);
    *pos = spos;
    return line.substr(spos, pend-spos);
}

//Find date of file creation
void readDateOfFile(std::string path, deviceInfo *device){
    std::string slash = "/";
    size_t pos=path.find(slash);
    size_t prev_pos = 0;
    if(pos==std::string::npos){
        slash = "\\";
        pos = path.find(slash);
        if(pos == std::string::npos){
            prev_pos = 0;
            slash = "/";
        }
    }
    while(pos!=std::string::npos){
        prev_pos = pos;
        pos = path.find(slash, pos+1);
    }
    pos = prev_pos+1;
    int year = std::stol(path.substr(pos, 4));
    int month = std::stol(path.substr(pos+5, 2));
    int day = std::stol(path.substr(pos+8, 2));
    #ifdef _DEBUG
    std::cout<<"year: "<<year<<", month: "<<month\
    <<", day: "<<day<<std::endl;
    #endif
    device->cvDate(
        day,
        month,
        year
    );
}

//Find device info
void readDeviceSN(std::string line, deviceInfo *device){
    size_t pos = 0;
    std::string keyword = "serial_number";
    device->setSerialNumber(extractValue(line, keyword, &pos));
}

void readDeviceSV(std::string line, deviceInfo *device){
    size_t pos = 0;
    std::string keyword = "software_version";
    device->setSoftwareVersion(extractValue(line, keyword, &pos));
}

//Finds GPS altitude in provided line.
void readGPSAlt(std::string line, GPSdata *gpsdata){
    std::string keyword = "enhanced_altitude";
    size_t pos = 0;
    std::string value = extractValue(line, keyword, &pos);
    gpsdata->setAlt(std::stod(value.c_str()));
}

//Finds all needed GPS data points in line.
void readGPS(std::string line, GPSdata *gpsdata) {
    std::vector<std::string> keywords = {
        "timestamp",
        "position_lat",
        "position_long",
        "enhanced_altitude",//this in fact is baro-enhanced
    };
    std::vector<std::string> values;
    size_t pos = 0;
    //timestamp
    for(size_t i=0; i<keywords.size(); i++)
        values.push_back(extractValue(line, keywords.at(i), &pos));
    
    gpsdata->setTime(std::stol(values.at(0)));
    gpsdata->setLat(std::stol(values.at(1)));
    gpsdata->setLon(std::stol(values.at(2)));
    gpsdata->setPal(std::stol(values.at(3)));
}

void questionare(deviceInfo *device){
    std::string line;
    std::cout << "Pilot's name:\n> ";
    std::getline(std::cin>>std::ws, line);
    #ifdef _DEBUG
    std::cout<<line<<std::endl;
    #endif
    device->setPilot(line);
    line = "";

    std::cout << "Glider type:\n> ";
    std::getline(std::cin>>std::ws, line);
    #ifdef _DEBUG
    std::cout<<line<<std::endl;
    #endif
    device->setGliderType(line);
    line = "";

    std::cout << "Glider ID:\n> ";
    std::getline(std::cin>>std::ws, line);
    #ifdef _DEBUG
    std::cout<<line<<std::endl;
    #endif
    device->setGliderID(line);
    line = "";
    
    device->setGPSDatum(1);

    std::cout << "GPS device model:\n> ";
    std::getline(std::cin>>std::ws, line);
    #ifdef _DEBUG
    std::cout<<line<<std::endl;
    #endif
    device->setFlightRecorder(line);
    line = "";
}

uint8_t readFile() {
    std::vector<std::string> lines;
    std::string path;
    std::string opath;
    std::string data_line_template = "Data,8,record,";
    std::string data_GPSa_template = "Data,3,gps_metadata,";
    std::string data_deviceSN_template = "Data,0,file_id,\
serial_number,";
    std::string data_deviceSV_template = "Data,1,file_creator,\
software_version,";
    //Let the user specify path to the FIT file
    std::cout << "Specify path to CSV file:\n> ";
    std::cin >> path;
    std::ifstream file;
    std::ofstream output;
    // Try opening the file. If it fails, return 1.
    try {
        file.open(path.c_str(), std::ios::in);
    }
    catch (std::exception& exc) {
        //return "1 - Exception while opening file" error
        #ifdef _DEBUG
        //This message is shown only when Debug mode is selected
        std::cout << "Error 1: exception while loading the file:\n";
        std::cout << path <<std::endl;
        #endif // _DEBUG

        return 1;
    }

    //If the file was not opened return "2 - error while
    //loading the file" error.
    if (!file.is_open()) return 2;
    
    opath = path + ".igc";
    output.open(opath.c_str(), std::ios::out);
    if(!output.is_open()) return 3;

    //Load the file contents to the memory
    //Line of the file
    std::string line;
    //device info
    deviceInfo device;
    //gps data
    GPSdata dt;
    std::vector<GPSdata> data;

    //finding flight date
    readDateOfFile(path, &device);

    //finding device info - scan first 12 rows for device info
    for(int i=0; i<12; i++){
        std::getline(file, line);
        if(findSubstr(data_deviceSN_template, line))
            readDeviceSN(line, &device);
        if(findSubstr(data_deviceSV_template, line))
            readDeviceSV(line, &device);
    }

    //Filling missing data
    questionare(&device);

    //Saving device info to output file
    output << device.e2igc();

    //read file line by line
    while (file) {
        std::getline(file, line);
        //If the GPS data line was found, analyse it.
        if (findSubstr(data_line_template, line)){
            readGPS(line, &dt);
            output << dt.e2igc() << std::endl;
            data.push_back(dt);
            dt.clear();
            continue;
        }
        //If true GPS alt line was found, extract it.
        if (findSubstr(data_GPSa_template, line)){
            readGPSAlt(line, &dt);
            continue;
        }
    }
    output.close();
    file.close();
    return 0;
}

int main(int argc, char* argv[])
{
    #ifdef NDEBUG
    std::cout << "Hello World!\n";
    #endif // NDEBUG
    #ifdef _DEBUG
    std::cout << "Hello debug mode!\n";
    #endif // _DEBUG

    uint8_t rf = readFile();
    switch (rf) {
    case 0:
        std::cout << "Program has ended correctly"<<std::endl;
        break;
    case 1:
        std::cout << "Exception while loading the file." << std::endl;
        break;
    case 2:
        std::cout << "Could not open the file" << std::endl;
        break;
    }

    return 0;
}
