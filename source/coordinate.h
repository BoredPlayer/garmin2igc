#pragma once

#ifndef __COORDINATE_H__
#define __COORDINATE_H__

#include <iostream>
#include <math.h>
#include <string>
#include <fstream>
#include <iterator>
#include <vector>
#include <stdlib.h>
#include <sstream>
#include <iomanip>
#include <cstdint>

class coordinate {
public:
    //NOTE:
    // This line is required to make call:
    // coordinate A = coordinate(0, 0);

    // Creates an object containing degrees and minutes
    // of given coordinates. The object automatically
    // translates degrees to degrees and minutes and
    // is capable of returning them in form of double
    // values, as well as std::string formated in acco-
    // rdance to IGC file standard.
    coordinate() = default;
    // Creates an object containing degrees and minutes
    // of given coordinates. The object automatically
    // translates degrees to degrees and minutes and
    // is capable of returning them in form of double
    // values, as well as std::string formated in acco-
    // rdance to IGC file standard. Requires a coordinate
    // and type (0 - lattitude, 1 - longitude)
    coordinate(double x, char type);
    //Returns only degrees
    double getDegrees();
    //Returns only minutes (with fractions)
    double getMinutes();
    // Returns string containing coordinates in accordance with
    // IGC file standard.
    std::string getString();
private:
    double complete_coordinate;
    double degrees;
    double minutes;
    char type;//0 - lat, 1 - lon
    char sign;
};

#endif // !__COORDINATE_H__