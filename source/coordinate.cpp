#include "coordinate.h"

coordinate::coordinate(double x, char type) {
    this->complete_coordinate = x;
    this->degrees = abs((int)x);
    this->minutes = fabs((x - this->degrees) * 60);
    this->type = type;
    //assign letter for hemisphere decoding
    if (this->type == 0) {
        if (x > 0) this->sign = 'N';
        else this->sign = 'S';
    }
    else {
        if (x > 0) this->sign = 'E';
        else this->sign = 'W';
    }
}

double coordinate::getDegrees() {
    return this->degrees;
}

double coordinate::getMinutes() {
    return this->minutes;
}

std::string coordinate::getString() {
    std::stringstream ms;
    ms << std::setw((this->type)?3:2) << std::setfill('0');
    ms << int(this->degrees);
    ms << std::setw(5) << int(this->minutes * 1000);
    ms << this->sign;
    return ms.str();
}