#pragma once

#ifndef __GPSDATA_H__
#define __GPSDATA_H__

#include <iostream>
#include <math.h>
#include <string>
#include <fstream>
#include <iterator>
#include <vector>
#include <stdlib.h>
#include <sstream>
#include <iomanip>
#include <cstdint>

#include "coordinate.h"

class GPSdata {
public:
    GPSdata();
    // Stores time of measurement, latitude,
    // longitude, altitude and pressure altitude
    // of the point. The time is automatically
    // translated from senconds to time of day.
    GPSdata(
        uint32_t timestamp,
        int32_t lat,
        int32_t lon,
        double alt,
        double pres_alt
    );
    // Clears data in object
    void clear();
    // Sets time stamp of the given point
    void setTime(uint32_t timestamp);
    void setLat(int32_t lat);
    void setLon(int32_t lon);
    void setAlt(double alt);
    void setPal(double pal);
    void set2Alt(double alt, double pal);
    void setDate(char day, char month, char year);
    std::string e2igc();
    std::string date2str();
private:
    uint32_t timestamp;
    uint32_t day;
    uint32_t month;
    uint32_t year;
    uint32_t hour;
    uint32_t minute;
    uint32_t second;
    //lattitude
    coordinate lat;
    //longitude
    coordinate lon;
    //alttitude
    double alt;
    //baro alttitude
    double pal;
    //Translates semicircles to degrees
    static double semi2deg(uint32_t x);
};

#endif // !__GPSDATA_H__