#include "GPSdata.h"

GPSdata::GPSdata(){
    this->clear();
}

GPSdata::GPSdata(
    uint32_t timestamp = 0,
    int32_t lat = 0,
    int32_t lon = 0,
    double alt = 0.,
    double pres_alt = 0.
) {
    this->setTime(timestamp);
    this->setLat(lat);
    this->setLon(lon);
    this->set2Alt(alt, pal);
}

void GPSdata::clear(){
    this->setTime(0);
    this->setLat(0);
    this->setLon(0);
    this->set2Alt(0., 0.);
}

double GPSdata::semi2deg(uint32_t x) {
    return (double)x * 180.0 / pow(2., 31.);
}

void GPSdata::setTime(uint32_t timestamp) {
    this->timestamp = timestamp;
    this->hour = (timestamp / 3600) % 24;
    this->minute = (timestamp / 60) % 60;
    this->second = timestamp % 60;
}

void GPSdata::setLat(int32_t lat) {
    this->lat = coordinate(this->semi2deg(lat), 0);
}

void GPSdata::setLon(int32_t lon) {
    this->lon = coordinate(this->semi2deg(lon), 1);
}

void GPSdata::setAlt(double alt) {
    this->alt = alt;
}

void GPSdata::setPal(double pal) {
    this->pal = pal;
}

void GPSdata::set2Alt(double alt, double pal) {
    this->alt = alt;
    this->pal = pal;
}

void GPSdata::setDate(char day, char month, char year){
    this->day = day;
    this->month = month;
    this->year = year;
}

std::string GPSdata::e2igc(){
    std::stringstream ms;
    //setup precision
    ms << "B";
    ms << std::setw(2) << std::setfill('0');
    //export time stamp
    ms << this->hour;
    ms << std::setw(2) << std::setfill('0');
    ms << this->minute;
    ms << std::setw(2) << std::setfill('0');
    ms << this->second;
    ms << this->lat.getString();
    ms << this->lon.getString();
    ms << "A";
    if(this->pal>0){
        ms << std::setw(5) << std::setfill('0');
        ms << (int)this->pal;
    }
    else{
        ms << "-";
        ms << std::setw(4) << std::setfill('0');
        ms << (int)fabs(this->pal);
    }
    if(this->alt>0){
        ms << std::setw(5) << std::setfill('0');
        ms << (int)this->alt;
    }
    else{
        ms << "-";
        ms << std::setw(4) << std::setfill('0');
        ms << (int)fabs(this->alt);
    }
    return ms.str();
}

std::string GPSdata::date2str(){
    std::stringstream ms;
    ms << std::setw(2) << std::setfill('0');
    ms << this->day;
    ms << std::setw(2) << std::setfill('0');
    ms << this->month;
    ms << std::setw(4) << std::setfill('0');
    ms << this->year;
    return ms.str();
}